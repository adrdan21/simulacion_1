<?php
$constante = $_POST["constante"];
$valor = $_POST["numero"];
$fijarValor = $valor;

$filas = $_POST["filas"];
$filas = $filas + 1;
$n = 0;
//$extrae = $valor;

$acumulador = 0;
$acumulaExtrae = [];
$aleatorio = 0;
?>

<!DOCTYPE html>
<html lang='en'>

<head>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
</head>

<body>

    <div>
        <h3 align="center">Multiplicador Constante <?php echo " con el número [" . $valor . "] y [" . $constante . "] como constante" ?></h3>

        <br>
        <h1 align="center"> <?php echo "K " . $constante; ?></h1>
        <br>

        <h4 align="center">Resutados</h4>

    </div>

    <table border='3' align='center'>
        <tr>
            <th>Xn </th>
            <th>K * (Xn-1) </th>
            <th>Extrae</th>
            <th>Número aleatorio </th>
        </tr>
        <?php
        for ($i = 0; $i < $filas; $i++) {
            if ($i === 0) {
        ?>
                <tr>
                    <td> <?php echo "X" . $n++; ?> </td>
                    <td></td>
                    <td><?php echo $valor ?></td>
                    <td></td>
                </tr>

                <?php
                $acumulaExtrae[$i] = $valor;
            } else {

                if ($i > 0) {
                    $acumulaExtrae[$i] = $valor;

                    $acumulador =   $acumulaExtrae[$i -1 ] * ($constante);

                    $parametroExtrae1 = $acumulador;
                    $parametroExtrae2 = floatval(floor((floatval(strlen($acumulador)) - floatval(strlen($constante))) / 2) + 1);
                    $parametroExtrae3 = strlen($fijarValor);
                    $extrae = (substr($parametroExtrae1, $parametroExtrae2 - 1, $parametroExtrae3)) . PHP_EOL;

                    $extrae = floatval($extrae);
                    $acumulaExtrae[$i] = $extrae;
                    $valor = $extrae;
                    $acumulaExtrae[$i] = $valor;

                    $aleatorio = $extrae / (pow(10, intval(strlen($valor))));


                ?> <tr>
                        <td> <?php echo "X" . $n++; ?> </td>
                        <td><?php echo  $acumulador; ?></td>
                        <td><?php echo $valor ?></td>
                        <td><?php echo $aleatorio ?></td>
                    </tr>

        <?php


                }
            }
        }

        ?>

    </table>

    <br>
    <div class="col-sm-12">
      <hr/>
      <table align="center">
        <tr>
          <td>
            <input class="btn btn-lg btn-secondary" value="Volver" onClick="history.go(-1);">
          </td>
        </tr>
      </table>
    </div>



</body>

</html>