<div class="container">
    <div class="row">
      <h1>Pruebas de Medias</h1>
        <div id="formulario" class="col-sm-12">
            <form id="form_lineal" align="center">
              
                <div class="row" align="left"> 
                    <div class="col-sm-3">
                       <label>Cantidad de numeros:</label><br>
                       <input id="filas" type="number" name="filas" required>
                    </div>
                    <div class="col-sm-3">
                       <label>Cantidad de Intervalos:</label><br>
                       <input id="intervalo" type="number" name="intervalo" required>
                    </div>
                    <div class="col-sm-3">
                       <label>Cantidad de decimales:</label><br>
                       <input id="decimales" type="number" name="decimales" required>
                    </div>
                    <div class="col-sm-3">
                       <label>Grados de Libertad:</label><br>
                       <input id="grados_lib" type="number" name="grados_lib" disabled>
                    </div>
                    <div class="col-sm-3">
                       <label>Nivel de Confianza:</label><br>
                       <input id="nivel_conf" type="number" name="nivel_conf">
                    </div>
                    <div class="col-sm-3">
                       <label>Valor a sumar intervalo 1:</label>
                       <input id="numero1" type="number" name="numero1" maxlength="5" size="5" required><br>
                    </div>
                    <div class="col-sm-3">
                       <label>Valor a sumar intervalo 2:</label>
                       <input id="numero2" type="number" name="numero2" maxlength="5" size="5" required><br>
                    </div>
                </div>
                <hr/>
                <span id="info" class="hide">
                  Respuesta: Deacuerdo al valor promedio de los numeros pseudoaleatorios igual a <input style="border: none" maxlength="5" size="5" type="text" name="prom" id="prom"> se encuentra entre los limites de aceptacion, podemos concluir que no podemos rechazar que el conjunto de <input style="border: none" maxlength="2" size="2" type="text" name="cant_numeros" id="cant_numeros"> numeros  r, tiene un valor esperado de <input style="border: none" maxlength="3" size="3" type="text" name="promedio2" id="promedio2"> con un nivel de aceptacion del 90%
                </span>
                <br><br>
                <div>
                   <input type="button" class="btn btn-sm btn btn-success" onclick="Generar()"  value="Generar">
                </div>
            </form>
        </div>
    </div>
    <hr/>
    <div class="row">
      <table id="tabla_chi" width="50%" border="2" align="center">
        <thead></thead>
      </table>
    </div>
    <hr/>
    <div class="row hide" id="solucion_form">
      <table id="tabla_solucion" width="50%" border="2" align="center">
        <thead>
         <tr>
           <th>Xn</th>
           <th>Intervalo</th>
           <th>Oi</th>
           <th>Ei</th>
           <th>(Ei-Oi)2 / Ei</th>
         </tr>

        </thead>
        <tbody></tbody>
     </table>
    </div>
</div>
<script type="text/javascript">

  function Generar(){
    if($('#filas').val() != ""){
      $('#info').show();
      $('#solucion_form').removeClass('hide');

      var x = 0;
      var y = [];
      var z = [];
      var Oi = 0;
      var Ei = parseFloat($('#filas').val() / $('#intervalo').val());
      var contar = 0;
      var num_decimales = $('#decimales').val();
      var ini = 1;
      var val_inicial = "0.";
      var html2 = '<tr class"text-center"><th>';
      var numero1 = $('#numero1').val();
      var numero2 = $('#numero2').val();

      for (var i = 0; i < num_decimales; i++) {
        ini+= "0";
        val_inicial+= "0";
      }

      for (var j = 0; j < $('#filas').val(); j++){
        x = parseInt((Math.random() * (ini - 0) + 0));
        z[i] = parseFloat(x / ini);
        z[i] = z[i].toFixed($('#decimales').val());
        html2 += "[" +z[i]+"]";
      }
      html2 += '</th></tr>';
      $('#tabla_chi thead').append(html2);

      var html = '';

      for (var k = 0; k < $('#intervalo').val(); k++) {
          if(k == 0){
            y[k] = $('#numero2').val();
            for(var l = 0; l < $('#filas').val(); l++ ){
              if(y[k] == z[l]){
                contar++;
              }
            }
            html += "<tr><td>X_" +k+ "</td><td>" +val_inicial+ " - " +  $('#numero2').val() +"</td><td>"+contar+"</td></td><td>"+Ei+"</td></tr>";
          }else{
            y[k] = 0;
            y[k] = parseFloat(y[k-1]) + parseFloat(numero1);
            for(var l = 0; l < $('#filas').val(); l++ ){
              if(y[k] == z[l]){
                contar++;
              }
            }
            html += "<tr><td>X_" +k+ "</td><td>" +y[k]+ " - " +  $('#numero2').val() +"</td><td>"+contar+"</td></td><td>"+Ei+"</td></tr>";
          }
          
      }

      $('#tabla_solucion tbody').append(html);

    }else{
      jQuery.gritter.add({title: 'Sistema', text: "Diligenciar todos los campos", time: '3000', class_name: "gritter-error"});
    }
  }

</script>

