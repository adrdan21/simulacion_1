<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <form action="productos_medios.php" method="POST" align="center">
              <div>
                <h1>Calculadora Productos Medios</h1>
                <label for="balloons">Valor inicial 1:</label>
                <input id="numeroPrimero" type="number" name="numeroPrimero" required>
                <label for="balloons">Valor inicial 2:</label>
                <input id="numeroSegundo" type="number" name="numeroSegundo" required>
                <br>
                <label for="balloons">Cantidad de filas:</label>
                <input id="filas" type="number" name="filas" required>
                <span class="validity"></span>
              </div>
              <br>
              <div>
                  <input type="submit" class="btn btn-sm btn btn-success"  value="Generar">
              </div>
          </form>
        </div>
    </div>
</div>
<script type="text/javascript">

</script>

