<div class="container">
    <div class="row">
      <h1>Algoritmo de BLUM, BLUM Y SHUB</h1>
        <div id="formulario" class="col-sm-12">
            <form id="form_lineal" align="center">
              
                <div class="row" align="left">                  
                    <div class="col-sm-3">
                       <label>Valor inicial:</label>
                       <input id="numero" type="number" name="numero" maxlength="5" size="5" required><br>
                    </div>
                    <div class="col-sm-3">
                       <label>Modulo:</label>
                       <input id="const_m" type="number" name="const_m" maxlength="5" size="5">
                       <span class="validity"></span>
                    </div>
                    <div class="col-sm-3">
                       <label>Cantidad de filas:</label>
                       <input id="filas" type="number" name="filas" required>
                    </div>
                </div>
                <br>
                <div>
                   <input type="button" class="btn btn-sm btn btn-success" onclick="Generar()"  value="Generar">
                </div>
            </form>
        </div>
    </div>
    <hr/>
    <div class="row hide" id="solucion_form">
      <table id="tabla_solucion" width="50%" border="2" align="center">
        <thead>
         <tr>
           <th>Xn</th>
           <th>(aXn-1²) mod m </th>
           <th>Número aleatorio</th>
         </tr>

        </thead>
        <tbody></tbody>
     </table>
    </div>
</div>
<script type="text/javascript">

  function Generar(){
    if($('#numero').val() != "" && $('#const_m').val() != ""){

      $('#solucion_form').removeClass('hide');

      var m = $('#const_m').val();
      var numero = $('#numero').val();

      var x = 0;
      var y = [];
      var m_1 = parseFloat(m-1);
      var aux = 0;
      var aux2 = 0;

      for (var i = 0; i <= $('#filas').val(); i++) {
        if(i == 0){
          y[i] = $('#numero').val();
          html = "<tr><td>X_" +i+ "</td><td>" + $('#numero').val() + "</td><td></td></tr>";
        }else{
          var potencia = Math.pow(y[i-1],2);
          aux = potencia%m;       
          aux2 = aux/m_1;
          y[i] = aux;
          html += "<tr><td>X_" +i+ "</td><td>" + aux + "</td><td>" + aux2 + "</td></tr>";
        }
        
      }
      $('#tabla_solucion tbody').append(html);

    }else{
      jQuery.gritter.add({title: 'Sistema', text: "Diligenciar todos los campos", time: '3000', class_name: "gritter-error"});
    }
  }

</script>

