<div class="container">
    <div class="row">
      <h1>Alogaritmo Congruencial Aditivo</h1>
        <div id="formulario" class="col-sm-12">
            <form id="form_lineal" align="center">
              
                <div class="row" align="left">                  
                    <div class="col-sm-3">
                       <label>Numero de valores iniciales:</label>
                       <input id="numero" type="number" name="numero" maxlength="5" size="5" required>
                       <button class="btn btn-sm btn btn-primary" onclick="agregarSemillas()">+</button>
                    </div>
                    <div id="num_semillas" class="col-sm-12 hide">
                       
                    </div>
                    <div class="col-sm-3">
                       <label>Modulo:</label>
                       <input id="const_m" type="number" name="const_m" maxlength="5" size="5" required>
                       <span class="validity"></span>
                    </div>
                    <div class="col-sm-3">
                       <label>Cantidad de filas:</label>
                       <input id="filas" type="number" name="filas" required>
                    </div>
                </div>
                <br>
                <div>
                   <input type="button" class="btn btn-sm btn btn-success" onclick="Generar()"  value="Generar">
                </div>
            </form>
        </div>
    </div>
    <hr/>
    <div class="row hide" id="solucion_form">
      <table id="tabla_solucion" width="50%" border="2" align="center">
        <thead>
         <tr>
           <th>Xn</th>
           <th>(Xn-1 + Xn-4) mod m</th>
           <th>Número aleatorio</th>
         </tr>

        </thead>
        <tbody></tbody>
     </table>
    </div>
</div>
<style type="text/css">
  table{
    padding-left: 15px;
  }
</style>
<script type="text/javascript">

  function agregarSemillas(){
    var num_semillas = $('#numero').val();
    var html2 = "";
    for(var j = 1; j <= num_semillas; j++){
      html2 += "<input id='numero_"+ j +"' type='number' name='numero_"+ j +"' maxlength='5' size='5' required>";
    }
    $('#num_semillas').removeClass('hide');
    $('#num_semillas').append(html2);
  }

  function Generar(){
    if($('#numero').val() != "" && $('#const_m').val() != ""){


      $('#solucion_form').removeClass('hide');

      var m = $('#const_m').val();
      var m_1 = parseFloat(m -1);
      var numero = $('#numero').val();

      var x = 0;
      var y = [];
      var k = 0;
      var aux = 0;
      var aux2 = 0;
      var aux3 = 0;
      var html = "";
      
      for (var j = 1; j <= numero; j++){
        html += "<tr><td>X_" +j+ "</td><td>" + $('#numero_'+j).val() + "</td><td></td></tr>";
      }  
      for (var i = 1; i <= $('#filas').val(); i++) { 
        aux2 = parseFloat(i) + parseFloat(numero) ;
        if(i == 1){
          x = parseFloat($('#numero_'+i).val()) + parseFloat($('#numero_'+numero).val());  
          y[i] = x;
          aux = x%m;
          aux3 = parseFloat(aux / m_1);
        }else if (i <= numero){
          x = parseFloat($('#numero_'+i).val()) + parseFloat(x);
          aux = x%m;
          y[i] = x;
          aux3 = parseFloat(aux / m_1);
        }else{
          k = y[i-1];
          x = parseFloat(k) + parseFloat(y[i-numero]);          
          aux = x%m;
          y[i] = x;
          aux3 = parseFloat(aux / m_1);
        }
        html += "<tr><td>X_" + aux2 + "</td><td>" + aux + "</td><td>" + aux3 + "</td></tr>";
        
        
      }
      $('#tabla_solucion tbody').append(html);

    }else{
      jQuery.gritter.add({title: 'Sistema', text: "Diligenciar todos los campos", time: '3000', class_name: "gritter-error"});
    }
  }

</script>

