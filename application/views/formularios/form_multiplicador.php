<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <form action="multiplicador_constante.php" method="POST" align="center">
              <div>
                  <h1>Calculadora de Multiplicador Constante</h1>
                  <label for="balloons">Ingrese la constante (K)</label>
                  <input id="constante" type="number" name="constante" required>
                  <br>
                  <label for="balloons">Valor inicial:</label>
                  <input id="numero" type="number" name="numero" required>
                  <label for="balloons">Cantidad de filas:</label>
                  <input id="filas" type="number" name="filas" required>
                  <span class="validity"></span>
              </div>
              <br>
              <div>
                  <input type="submit" class="btn btn-sm btn btn-success"  value="Generar">
              </div>
          </form>
        </div>
    </div>
</div>
<script type="text/javascript">

</script>

