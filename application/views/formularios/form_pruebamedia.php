<div class="container">
    <div class="row">
      <h1>Pruebas de Medias</h1>
        <div id="formulario" class="col-sm-12">
            <form id="form_lineal" align="center">
              
                <div class="row" align="left"> 
                    <div class="col-sm-3">
                       <label>Media:</label>
                       <input type="number" name="media" id="media" value="0.5" disabled><br>
                    </div>   
                    <div class="col-sm-3">
                       <label>Varianza: σ² = 1/12</label><br>
                       <input type="number" name="varianza" id="varianza" value="0.08333" disabled><br>
                    </div> 
                    <div class="col-sm-3">
                       <label>Nivel de Aceptacion: 90%</label><br>
                       <input type="number" name="nivel_aceptacion" id="nivel_aceptacion" value="1.65" disabled><br>
                    </div>
                    <div class="col-sm-3">
                       <label>Promedio:</label>
                       <input type="number" name="promedio" id="promedio" disabled><br>
                    </div>              
                    <div class="col-sm-3">
                       <label>Cantidad de numeros:</label><br>
                       <input id="filas" type="number" name="filas" required>
                    </div>
                    <div class="col-sm-3">
                       <label>LI:</label><br>
                       <input id="li" type="number" name="li" disabled>
                    </div>
                    <div class="col-sm-3">
                       <label>LS:</label><br>
                       <input id="ls" type="number" name="ls" disabled>
                    </div>
                </div>
                <hr/>
                <span id="info" class="hide">
                  Respuesta: Deacuerdo al valor promedio de los numeros pseudoaleatorios igual a <input style="border: none" maxlength="5" size="5" type="text" name="prom" id="prom"> se encuentra entre los limites de aceptacion, podemos concluir que no podemos rechazar que el conjunto de <input style="border: none" maxlength="2" size="2" type="text" name="cant_numeros" id="cant_numeros"> numeros  r, tiene un valor esperado de <input style="border: none" maxlength="3" size="3" type="text" name="promedio2" id="promedio2"> con un nivel de aceptacion del 90%
                </span>
                <br><br>
                <div>
                   <input type="button" class="btn btn-sm btn btn-success" onclick="Generar()"  value="Generar">
                </div>
            </form>
        </div>
    </div>
    <hr/>
    <div class="row hide" id="solucion_form">
      <table id="tabla_solucion" width="50%" border="2" align="center">
        <thead>
         <tr>
           <th>Xn</th>
           <th>Número aleatorio</th>
         </tr>

        </thead>
        <tbody></tbody>
     </table>
    </div>
</div>
<script type="text/javascript">

  function Generar(){
    if($('#filas').val() != ""){
      $('#info').show();
      $('#solucion_form').removeClass('hide');

      var x = 0;
      var y = [];
      var promedio = 0;
      var r = 0;
      var li = 0;
      var ls = 0;
      var nivel_acep = $('#nivel_aceptacion').val();
      var html = '';

      for (var i = 0; i < $('#filas').val(); i++) {
          y[i] = Math.random() * (1 - 0) + 0;
          x = x + y[i]
          html += "<tr><td>X_" +i+ "</td><td>" + y[i] + "</td></tr>";  
      }
      promedio = (x / $('#filas').val());
      li = (1/2)- ((parseFloat(nivel_acep)) * (1/ Math.sqrt(12*$('#filas').val())));
      ls = (1/2)+ ((parseFloat(nivel_acep)) * (1/ Math.sqrt(12*$('#filas').val())));
      r = (li+ls) / 2;
      $('#promedio').val(r);
      $('#promedio2').val(r);
      $('#prom').val(promedio);
      $('#cant_numeros').val($('#filas').val());
      $('#li').val(li);
      $('#ls').val(ls);
      $('#tabla_solucion tbody').append(html);

    }else{
      jQuery.gritter.add({title: 'Sistema', text: "Diligenciar todos los campos", time: '3000', class_name: "gritter-error"});
    }
  }

</script>

