<div class="container">
    <div class="row">
      <h1>Algoritmo Congruencial Multiplicativo</h1>
        <div id="formulario" class="col-sm-12">
            <form id="form_lineal" align="center">
              
                <div class="row" align="left">                  
                    <div class="col-sm-3">
                       <label>Valor inicial:</label>
                       <input id="numero" type="number" name="numero" maxlength="5" size="5" required onkeyup="add_numero()"><br>
                       <span style="color: red" class="validar_numero"></span>
                    </div>
                    <div class="col-sm-3">
                       <label>Constante Multiplicativa:</label>
                       <input disabled id="const_a" type="number" name="const_a" maxlength="5" size="5" required>
                    </div>
                    <div class="col-sm-3">
                       <label>Modulo:</label>
                       <input disabled id="const_m" type="number" name="const_m" maxlength="5" size="5">
                       <span class="validity"></span>
                    </div>
                    <div class="col-sm-3">
                       <label>Valor G:</label>
                       <input id="g" type="number" name="g" required onkeyup="add_m()"><br>
                       <span class="enteroG" style="color: red"></span>
                    </div>
                    <div class="col-sm-3">
                       <label>Valor K:</label>
                       <input id="k" type="number" name="k" required onkeyup="add_a()"><br>
                       <span class="enteroK" style="color: red"></span>
                    </div>
                    <div class="col-sm-3">
                       <label>Cantidad de filas:</label>
                       <input id="filas" type="number" name="filas" required>
                    </div>
                </div>
                <br>
                <div>
                   <input type="button" class="btn btn-sm btn btn-success" onclick="Generar()"  value="Generar">
                </div>
            </form>
        </div>
    </div>
    <hr/>
    <div class="row hide" id="solucion_form">
      <table id="tabla_solucion" width="50%" border="2" align="center">
        <thead>
         <tr>
           <th>Xn</th>
           <th>(aXn-1) mod (m)</th>
           <th>Número aleatorio</th>
         </tr>

        </thead>
        <tbody></tbody>
     </table>
    </div>
</div>
<script type="text/javascript">

  function add_numero(){
    $('.validar_numero').html("");
    var n1 = $('#numero').val();
    if(n1%2 != 1){
      $('.validar_numero').html("El valor ingresado no es un numero impar");
    }
  }

  function add_m(){
    $('.enteroG').html("");
    var g = $('#g').val();
    entero = esEntero(g);

    if(entero === true){
      g = Math.pow(2,g)
      $('#const_m').val(g);
    }else{
      $('.enteroG').html('G no es un numero entero');
    }
    
  }

  function add_a(){
    $('.enteroK').html("");
    var k = $('#k').val();
    entero = esEntero(k);

    if(entero === true){
      k = (3 + (8*k));
      $('#const_a').val(k);
    }else{
      $('.enteroK').html('K no es un numero entero');
    }
    
  }

  function Generar(){
    if($('#numero').val() != "" && $('#const_a').val() != "" && $('#const_m').val() != ""){

      $('#solucion_form').removeClass('hide');

      var a = $('#const_a').val();
      var m = $('#const_m').val();
      var numero = $('#numero').val();

      var x = 0;
      var y = [];
      var m_1 = parseFloat(m-1);
      var aux = 0;
      var aux2 = 0;

      for (var i = 0; i <= $('#filas').val(); i++) {
        if(i == 0){
          y[i] = $('#numero').val();
          html = "<tr><td>X_" +i+ "</td><td>" + $('#numero').val() + "</td><td></td></tr>";
        }else{
          x = parseFloat( y[i-1] * a );          
          aux = x%m;
          aux2 = aux/m_1;
          y[i] = aux;
          html += "<tr><td>X_" +i+ "</td><td>" + aux + "</td><td>" + aux2 + "</td></tr>";
        }
        
      }
      $('#tabla_solucion tbody').append(html);

    }else{
      jQuery.gritter.add({title: 'Sistema', text: "Diligenciar todos los campos", time: '3000', class_name: "gritter-error"});
    }
  }

</script>

