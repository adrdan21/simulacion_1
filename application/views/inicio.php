<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Simulación</title>
	<!--Js-->
	<script src="<?= base_url(); ?>jquery/dist/jquery.min.js"></script>
	<script src="<?= base_url(); ?>jquery/dist/jquery.js"></script>
	<!--Css-->
	<link rel="stylesheet" href="<?= base_url(); ?>bootstrap/css/bootstrap.min.css">
	<script src="<?= base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
    <!--Gritter-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>jquery/jquery.gritter.css">
    <script type="text/javascript" language="javascript" src="<?= base_url(); ?>jquery/jquery.gritter.js"></script> 
    <script type="text/javascript" language="javascript" src="<?= base_url(); ?>jquery/jquery.gritter.min.js"></script> 
    <link href="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css" rel="stylesheet"/>
	<script src="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>
	
</head>

<body id="content">
	<div class="container">
		<nav class="navbar navbar-dark bg-dark" >
			<div class="widget-header widget-header-small ui-corner-all">
	            <div class="widget-toolbar pull-left ">
	                <div class="btn-group btn-corner">
	                	<a id="inicio" class="navbar-brand itmenu" selected data_opc= "inicio" href="javascript:location.reload()">Inicio</a> 
	                	<div id="dropdown_registro" class="dropdown">
						    <a class="navbar-brand dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Logaritmos
						    </a>
						    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						    	<a class="dropdown-item itmenu" data_opc= "algo_cuadrosmedios" href="#">Algoritmo de Cuadros Medios</a>
						    	<a class="dropdown-item itmenu" data_opc= "algo_productosmedios" href="#">Algoritmo de Productos Medios</a>
						    	<a class="dropdown-item itmenu" data_opc= "algo_multiplicador" href="#">Algoritmo de Multiplicador constante</a>
						    	<a class="dropdown-item itmenu" data_opc= "algo_lineal" href="#">Algoritmo Lineal</a>
						    	<a class="dropdown-item itmenu" data_opc= "algo_congruencial_aditivo" href="#">Algoritmo Congruencial Aditivo</a>
						    	<a class="dropdown-item itmenu" data_opc= "algo_numeros_pseudoaleatorios" href="#">Algoritmo Congruencial Multiplicativo</a>
						    	<a class="dropdown-item itmenu" data_opc= "algo_numeros_congruencialcuadratico" href="#">Algoritmo Congruencial Cuadratico</a>
						    	<a class="dropdown-item itmenu" data_opc= "algo_numeros_blum" href="#">Algoritmo Blum</a>
						    	<a class="dropdown-item itmenu" data_opc= "prueba_media" href="#">Pruebas Media </a>
						    	<a class="dropdown-item itmenu" data_opc= "prueba_varianza" href="#">Pruebas Varianza </a>
						    	<a class="dropdown-item itmenu" data_opc= "prueba_chi" href="#">Pruebas Chi Cuadrado </a>
						  	</div>
						</div>
						<a id="inicio" class="navbar-brand itmenu" selected data_opc= "integrantes" href="#">Integrantes</a> 
	                </div>
	            </div>
        	</div>
  		</nav>
  		<hr/>
  		<div id="app" class="container">
  			<div class="row">
  				<div class="col-sm-12">
  					<h3 class="text-center">Simulación</h3>
  					<p>
  						Simulación es un sistema, el cual puede ser modelado a través de un diseño digital o físico para identificar los diferentes procesos que este pueda presentar y así analizar variables que posiblemente no se tuvieron en cuenta antes de estructurar el procedimiento real.<br>
  						<p style="padding-left: 15px"> 
  							Una de las definiciones más aceptadas y difundidas de la palabra simulación. Thomas H. Naylor la define así: <a href="https://books.google.com.co/books?id=iY6dI3E0FNUC&printsec=frontcover&dq=simulacion+un+enfoque+practico&hl=es&sa=X&ved=2ahUKEwiP0qXZjZjvAhVBp1kKHR4RC1sQ6AEwAHoECAAQAg#v=onepage&q=simulacion%20un%20enfoque%20practico&f=false">(Coss, 2003)</a>.<br>
							Simulación es una técnica numérica para conducir experimentos en una computadora digital. Estos experimentos comprenden ciertos tipos de relaciones matemáticas y lógicas, las cuales son necesarias para describir el comportamiento y la estructura de sistemas complejos del mundo real a través de largos periodos de tiempo. <a href="https://books.google.com.co/books?id=iY6dI3E0FNUC&printsec=frontcover&dq=simulacion+un+enfoque+practico&hl=es&sa=X&ved=2ahUKEwiP0qXZjZjvAhVBp1kKHR4RC1sQ6AEwAHoECAAQAg#v=onepage&q=simulacion%20un%20enfoque%20practico&f=false">(Coss, 2003)</a>
  						</p>
  					</p>
  				</div>
  			</div>
  		</div>
  		<br>
	</div>
	<hr/>
</body>

<footer class="page-footer font-small blue">
  	<div class="footer-copyright text-center py-3">
  		<p style="font-size: 12px; text-align: center;">
  			<b>
  			INSTITUTO TECNOLOGICO DEL PUTUMAYO<br>
  			FACULTAD DE INGENIERIA Y CIENCIAS BASICAS<br>
  			INGENIERIA DE SISTEMAS<br>
  			SIMULACION<br>
  			MOCOA- PUTUMAYO<br>
  			2021<br>
  			</b>
  		</p>
  	</div>
</footer>

</html>

<style type="text/css">

	.hide{
		display: none;
	}

    label {
		color: blue;
		font-weight: #137DED;
	}

</style>

<script type="text/javascript">

    $('.itmenu').on('click', function() {

		var opc = $(this).attr('data_opc');
		switch (opc) {
			case 'algo_cuadrosmedios':
				$("#app").load("index.php/welcome/form_cuadrosmedios")
				break;
			case 'algo_productosmedios':
				$("#app").load("index.php/welcome/form_productomedios")
				break;
			case 'algo_multiplicador':
				$("#app").load("index.php/welcome/form_multiplicador")
				break;
			case 'algo_lineal':
				$("#app").load("index.php/welcome/form_lineal")
				break;
			case 'algo_congruencial_aditivo':
				$("#app").load("index.php/welcome/form_congruencial_aditivo")
				break;		
			case 'algo_numeros_pseudoaleatorios':
				$("#app").load("index.php/welcome/form_numeros_pseudoaleatorios")
				break;		
			case 'integrantes':
				$("#app").load("index.php/welcome/listar_integrantes")
				break;
			case 'algo_numeros_congruencialcuadratico':
				$("#app").load("index.php/welcome/form_congruencialcuadratico")
				break;
			case 'algo_numeros_blum':
				$("#app").load("index.php/welcome/form_blum")
				break;
			case 'prueba_media':
				$("#app").load("index.php/welcome/form_pruebamedia")
				break;
			case 'prueba_varianza':
				$("#app").load("index.php/welcome/form_pruebavarianza")
				break;
			case 'prueba_chi':
				$("#app").load("index.php/welcome/form_chi")
				break;
		}
	});
	
</script>
