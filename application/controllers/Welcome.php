<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct() {
        parent::__construct();
    }

	public function index(){	
		$this->load->view('inicio');
	}

	public function listar_integrantes(){	
		$this->load->view('listar_integrantes');
	}

	public function form_cuadrosmedios(){
		$this->load->view('formularios/form_cuadrados_medios');
	}

	public function form_productomedios(){
		$this->load->view('formularios/form_productomedios');
	}

	public function form_multiplicador(){
		$this->load->view('formularios/form_multiplicador');
	}

	public function form_lineal(){
		$this->load->view('formularios/form_lineal');
	}

	public function form_congruencial_aditivo(){
		$this->load->view('formularios/form_congruencial_aditivo');
	}

	public function form_numeros_pseudoaleatorios(){
		$this->load->view('formularios/form_numeros_pseudoaleatorios');		
	}

	public function form_congruencialcuadratico(){
		$this->load->view('formularios/form_congruencialcuadratico');		
	}
	
	public function form_blum(){
		$this->load->view('formularios/form_blum');
	}

	public function form_pruebamedia(){
		$this->load->view('formularios/form_pruebamedia');
	}

	public function form_pruebavarianza(){
		$this->load->view('formularios/form_pruebavarianza');
	}

	public function form_chi(){
		$this->load->view('formularios/form_chi');
	}

}
