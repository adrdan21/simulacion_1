/* hyoG Rev 29112018 <= CON $(DOCUMENT) COMO ARRANQUE => */
$(document).ajaxStart(function () {
    $('.btn_submit_protected').prop("disabled", true);
    $('.page-content').block({
        centerY: 0,
        css: {
            top: '10px', left: '', right: '10px', border: 'none', padding: '15px', backgroundColor: '#fff',
            '-webkit-border-radius': '10px', '-moz-border-radius': '10px', opacity: .5, color: '#000'},
        message: '<h1> <img src="' + Url_Files_Server + 'img/botones/cargando.gif"> Un Momento</h1>',
        overlayCSS: {backgroundColor: '#FAFAFA'}
    });
});

$(document).ajaxStop(function () {
    setTimeout(function () {
        $('.btn_submit_protected').prop("disabled", false);
    }, 1000);

    $('.page-content').unblock();
});

$(document).ajaxError(function (event, jqxhr, settings, exception) {
    $('.btn_submit_protected').prop("disabled", false);
    $('.page-content').unblock();
    var ajxErr = jqxhr.status + "";
    ajxErr = ajxErr.indexOf('401');
    ajxErr = (ajxErr < 0) ? false : true;
    var urlErr=settings.url+"";
    urlErr = urlErr.indexOf('Auditoria/jx_registrar_log');
    urlErr = (urlErr < 0) ? true : false;
    if (exception != 'abort' && !ajxErr && urlErr) {
        $('#icon_bugs').removeClass('green');
        $('#icon_bugs').addClass('orange');
        $("#ErrorLog").html($("#ErrorLog").html() + "<span class='small'><b>" + jqxhr.statusText + "</b><br>" + jqxhr.responseText + "</span>");
        $("#ErrorLog").find('style').remove('style');
        document.getElementById('sonido_not').play();
        
        var Obj_audi_error = {
            n_error: jqxhr.status,
            msg: jqxhr.statusText,
            url: settings.url,
            data_sent:JSON.stringify(settings),
            obj_err: JSON.stringify(jqxhr),
            base: GLO_js_storage_id,
            user: GLO_js_user_id
        };

        $.ajax({ data: Obj_audi_error, type: 'POST',url: URL_AUDI_LOG});
        
    } else if (ajxErr) {
        inactividad();
    }
});

//activar ayuda con la tecla F1
$(document).keydown(function (tecla) {
    if (tecla.keyCode == 27) {
        $('.ui-tooltip').hide();
        $('.popover').popover('destroy');
    }
    if (tecla.keyCode == 112) {
        MostrarAyuda();
        return false;
    }
});

if (isMovil) {
    var btndown = "<button id='btn-mov-arriba' type='button' class='btn btn-app btn-inverse btn-xs' style='position:fixed;left:" + ($(window).width() / 2) + "px; top:0px; width:40px; height:40px;'><i class='fa fa-chevron-up'></i></button>";
    btndown += "<button id='btn-mov-abajo' type='button' class='btn btn-app btn-primary btn-xs' style='position:fixed;left:" + ($(window).width() / 2) + "px; top:" + ($(window).height() - 40) + "px; width:40px; height:40px;'><i class='fa fa-chevron-down'></i></button>";
    $('body').append(btndown);
    $('#btn-mov-abajo').click(function () {
        if (($(window).scrollTop() * 1) < (($(document).height() * 1) - ($(window).height() * 1))) {
            $('body, html').animate({scrollTop: ($(window).scrollTop() + $(window).height())}, 100);
        }
    });
    $('#btn-mov-arriba').click(function () {
        if ($(window).scrollTop() > 0) {
            $('body, html').animate({scrollTop: ($(window).scrollTop() - $(window).height())}, 100);
        }
    });

    $(window).on("orientationchange", function (event) {
        setTimeout(function () {
            $('#btn-mov-arriba').css({left: ($(window).width() / 2) + "px"});
            $('#btn-mov-abajo').css({left: ($(window).width() / 2) + "px", top: ($(window).height() - 40) + "px"});
        }, '2000');
    });
}

$(document).ready(function () {
    /* hyoG Rev 29112018 Este if estaba al final separado.*/
    if (glo_rol_personal == "12" || glo_rol_personal == "7" || glo_rol_personal == "8" || glo_rol_personal == "9" || glo_rol_personal == "14" || glo_rol_personal == "105" || glo_rol_personal == "19") {
        if ($("#btn_app_inicial").length) {
            $("#btn_app_inicial").trigger("click");
        }
        /*          $('body').css('zoom', '110%');
         $('body').css('MozTransform','scale(1.1)');
         $('body').css('font-size', '14px'); */
    }
});

/* FIN hyoG Rev 29112018 <= CON $(DOCUMENT) COMO ARRANQUE => */

/* hyoG Rev 29112018 <= PROCEDIMIENTOS SOBRE ELEMENTOS PUNTUALES => */
$('html').click(function (e) {
    if (!$(e.target).hasClass('permitir_popover') && !$(e.target).parent().hasClass('permitir_popover')) {
        $('.popover').popover('destroy');
    }
});

$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
    _title: function (title) {
        var $title = this.options.title || '&nbsp;'
        if (("title_html" in this.options) && this.options.title_html == true)
            title.html($title);
        else
            title.text($title);
    }
}));

$("a.menup").on("click", function (e) {
    e.preventDefault();
    var self = $(this);
    $('.menu_l').removeClass("active");
    $("ul.navbar-nav > li").removeClass("active");
    $(this).parent().addClass("active");
    $.ajax({
        url: rutaAbs + "" + self.attr("href"),
        type: 'POST',
        success: function (data) {
            $("#contenedor").html(data);
            VolverForm();
        }
    });
});

$("a.menur").on("click", function (e) {
    e.preventDefault();
    var self = $(this);
    $('.menu_l').parent().parent().removeClass("active");
    $('.menu_l').removeClass("active");
    $(this).parent().parent().parent().addClass("active");
    $(this).parent().addClass("active");
    //$('li').removeClass("hover-show");
    //$('li').removeClass("hover-shown");
    $('#contenedor').html("");
    $.ajax({
        url: rutaAbs + "scai_arranque/" + self.attr("href") + "/" + self.attr("data-vid"),
        type: 'POST', dataType: 'html',
        success: function (data) {
            $("#menurib").html(data);
            VolverForm();
        }
    });
});

//Accionar acceso rapido fixed
$("#footer_gal").toggle();
$(".href_redir").on('click', function (e) {
    e.preventDefault();
    var self = $(this);
    if (self.attr("data-mode") * 1 == 1) {
        $("#contenedor").load(rutaAbs + self.attr("href"));
        VolverForm();
    }
    if (self.attr("data-mode") * 1 == 0) { //div active
        //$("#" + self.attr("href")).toggle();
        $("#footer_gal").load(rutaAbs + self.attr("href")).toggle();
    }
    if (self.attr("data-mode") * 1 == 2) {
        $("#contenedor").load(rutaAbs + self.attr("href"));
    }
    ShowGeneralOptions();
});
/* FIN hyoG Rev 29112018 <= PROCEDIMIENTOS SOBRE ELEMENTOS PUNTUALES => */

/* hyoG Rev 29112018 <= VALIDACIONES GENERALES => */
/*
 $.validator.addMethod('TimeAMPM', function(value) {
 return /^(0?[1-9]|1[012])(:[0-5]\d)[APap][mM]$/.test(value);
 }, 'Utilice el siguiente formato: ' + moment().format('hh:mma')); */

$.validator.addMethod('TimeDuracionseg', function (value) {
    return /^([0-9]{2}):[0-5][0-9]$/.test(value);
}, 'Utilice el siguiente formato: mm:ss');

$.validator.addMethod('TimeDuracion', function (value) {
    return /^([0-9]{2}):[0-5][0-9]$/.test(value);
}, 'Utilice el siguiente formato: hh:mm');

$.validator.addMethod('cmbrequired', function (value) {
    if (value * 1 <= 0) {
        return false;
    } else {
        return true;
    }
}, 'Seleccione una opcion.');

$.validator.setDefaults({ignore: ":hidden:not(select)"});
/* FIN hyoG Rev 29112018 <= VALIDACIONES GENERALES => */