/* hyoG Rev 29112018 <= FUNCIONES => */

/* hyoG Rev 29112018 <= FUNCIONES QUE DEVUELVEN VALORES => */
function Glo_getCrFechasByAnios(n_fechas, direccion) {
    let diasInicio = "";
    let diasHasta = "";
    let anio_actual = moment().year();
    let fechas = [];
    let titulo = '';
    let prefijo_titulo = '';

    titulo = "Este " + moment().format('YYYY');
    if (direccion == "vencida" || direccion == "atras") {
        diasInicio = moment().year(anio_actual).startOf('year').format('YYYY-MM-DD');
        diasHasta = moment().subtract(1, 'day').format('YYYY-MM-DD');
        prefijo_titulo = 'De ';
    } else {
        diasInicio = moment().format('YYYY-MM-DD');
        diasHasta = moment().year(anio_actual).endOf('year').format('YYYY-MM-DD');
        prefijo_titulo = 'En ';
    }
    fechas.push([titulo, diasInicio, diasHasta]);

    if (n_fechas > 1) {
        for (var i = 1; i < n_fechas; i++) {
            if (direccion == "vencida" || direccion == "atras") {
                diasInicio = moment().year(anio_actual - i).startOf('year').format('YYYY-MM-DD');
                diasHasta = moment().year(anio_actual - i).endOf('year').format('YYYY-MM-DD');
                titulo = prefijo_titulo + moment().year(anio_actual - i).endOf('year').format('YYYY');
            } else {
                diasInicio = moment().year(anio_actual + i).startOf('year').format('YYYY-MM-DD');
                diasHasta = moment().year(anio_actual + i).endOf('year').format('YYYY-MM-DD');
                titulo = prefijo_titulo + moment().year(anio_actual + i).endOf('year').format('YYYY');
            }
            fechas.push([titulo, diasInicio, diasHasta]);
        }
    }
    return fechas;
}

function Glo_getCrFechasByTrimestres(n_fechas, direccion) {
    let diasInicio = "";
    let diasHasta = "";
    let trim_actual = moment().quarter();
    let fechas = [];
    let titulo = '';
    let prefijo_titulo = '';

    titulo = "Este Trimestre";
    if (direccion == "vencida" || direccion == "atras") {
        diasInicio = moment().quarter(trim_actual).startOf('quarter').format('YYYY-MM-DD');
        diasHasta = moment().subtract(1, 'day').format('YYYY-MM-DD');
        prefijo_titulo = 'Hace ';
    } else {
        diasInicio = moment().format('YYYY-MM-DD');
        diasHasta = moment().quarter(trim_actual).endOf('quarter').format('YYYY-MM-DD');
        prefijo_titulo = 'En ';
    }
    fechas.push([titulo, diasInicio, diasHasta]);

    if (n_fechas > 1) {
        for (var i = 1; i < n_fechas; i++) {
            if (i == 1) {
                titulo = prefijo_titulo + i + ' Trimestre';
            } else {
                titulo = prefijo_titulo + i + ' Trimestres';
            }

            if (direccion == "vencida" || direccion == "atras") {
                diasInicio = moment().quarter(trim_actual - i).startOf('quarter').format('YYYY-MM-DD');
                diasHasta = moment().quarter(trim_actual - i).endOf('quarter').format('YYYY-MM-DD');
            } else {
                diasInicio = moment().quarter(trim_actual + i).startOf('quarter').format('YYYY-MM-DD');
                diasHasta = moment().quarter(trim_actual + i).endOf('quarter').format('YYYY-MM-DD');
            }
            fechas.push([titulo, diasInicio, diasHasta]);
        }
    }
    return fechas;
}

function Glo_getCrFechasByMeses(n_fechas, direccion) {
    let diasInicio = "";
    let diasHasta = "";
    let mesactual = moment().month();
    let fechas = [];
    let titulo = '';
    let prefijo_titulo = '';

    titulo = "Este " + moment().locale('es').format('MMMM');
    if (direccion == "vencida" || direccion == "atras") {
        diasInicio = moment().month(mesactual).date(1).format('YYYY-MM-DD');
        diasHasta = moment().subtract(1, 'day').format('YYYY-MM-DD');
        prefijo_titulo = 'De ';
    } else {
        diasInicio = moment().format('YYYY-MM-DD');
        diasHasta = moment().month(mesactual).endOf('month').format('YYYY-MM-DD');
        prefijo_titulo = 'En ';
    }
    fechas.push([titulo, diasInicio, diasHasta]);

    if (n_fechas > 1) {
        for (var i = 1; i < n_fechas; i++) {
            if (direccion == "vencida" || direccion == "atras") {
                diasInicio = moment().month(mesactual - i).date(1).format('YYYY-MM-DD');
                diasHasta = moment().month(mesactual - i).endOf('month').format('YYYY-MM-DD');
                titulo = prefijo_titulo + moment().month(mesactual - i).endOf('month').locale('es').format('MMMM');
            } else {
                diasInicio = moment().month(mesactual + i).date(1).format('YYYY-MM-DD');
                diasHasta = moment().month(mesactual + i).endOf('month').format('YYYY-MM-DD');
                titulo = prefijo_titulo + moment().month(mesactual + i).endOf('month').locale('es').format('MMMM');
            }
            fechas.push([titulo, diasInicio, diasHasta]);
        }
    }
    return fechas;
}

function Glo_getCrFechasBySemanas(n_fechas, direccion) {
    let diasInicio = "";
    let diasHasta = "";
    let semanaactual = moment().week();
    let fechas = [];
    let titulo = '';
    let prefijo_titulo = '';

    titulo = "Esta Semana";
    if (direccion == "vencida" || direccion == "atras") {
        diasInicio = moment().week(semanaactual).isoWeekday(1).format('YYYY-MM-DD');
        diasHasta = moment().subtract(1, 'day').format('YYYY-MM-DD');
        prefijo_titulo = 'Hace ';
    } else {
        diasInicio = moment().format('YYYY-MM-DD');
        diasHasta = moment().week(semanaactual).isoWeekday(7).format('YYYY-MM-DD');
        prefijo_titulo = 'En ';
    }
    fechas.push([titulo, diasInicio, diasHasta]);

    if (n_fechas > 1) {
        for (var i = 1; i < n_fechas; i++) {
            if (i == 1) {
                titulo = prefijo_titulo + i + ' Semana';
            } else {
                titulo = prefijo_titulo + i + ' Semanas';
            }

            if (direccion == "vencida" || direccion == "atras") {
                diasInicio = moment().week(semanaactual - i).isoWeekday(1).format('YYYY-MM-DD');
                diasHasta = moment().week(semanaactual - i).isoWeekday(7).format('YYYY-MM-DD');
            } else {
                diasInicio = moment().week(semanaactual + i).isoWeekday(1).format('YYYY-MM-DD');
                diasHasta = moment().week(semanaactual + i).isoWeekday(7).format('YYYY-MM-DD');
            }
            fechas.push([titulo, diasInicio, diasHasta]);
        }
    }

    return fechas;
}

function Glo_getCrFechasByDias(n_fechas, direccion) {
    let diasInicio = "";
    let diasHasta = "";
    let fechas = [];
    let titulo = '';
    let prefijo_titulo = '';

    titulo = (direccion == "vencida" || direccion == "atras") ? "Ayer" : "Hoy";
    if (direccion == "vencida" || direccion == "atras") {
        diasInicio = moment().subtract(1, 'day').format('YYYY-MM-DD');
        diasHasta = moment().subtract(1, 'day').format('YYYY-MM-DD');
        prefijo_titulo = 'Hace ';
    } else {
        diasInicio = moment().format('YYYY-MM-DD');
        diasHasta = moment().format('YYYY-MM-DD');
        prefijo_titulo = 'En ';
    }
    fechas.push([titulo, diasInicio, diasHasta]);

    if (n_fechas > 1) {
        for (var i = 1; i < n_fechas; i++) {
            if (i == 1) {
                titulo = prefijo_titulo + i + ' Dia';
            } else {
                titulo = prefijo_titulo + i + ' Dias';
            }

            if (direccion == "vencida" || direccion == "atras") {
                diasInicio = moment().subtract(i, 'day').format('YYYY-MM-DD');
                diasHasta = moment().subtract(i, 'day').format('YYYY-MM-DD');
            } else {
                diasInicio = moment().add(i, 'day').format('YYYY-MM-DD');
                diasHasta = moment().add(i, 'day').format('YYYY-MM-DD');
            }
            fechas.push([titulo, diasInicio, diasHasta]);
        }
    }

    return fechas;
}

function TablaHTMLtoArray(IdTabla) {
    var ArregloTabla = [];
    $("#" + IdTabla + " thead tr").each(function () {
        var arrayOfThisRow = [];
        var tableData = $(this).find('th');
        if (tableData.length > 0) {
            tableData.each(function () {
                arrayOfThisRow.push($(this).text());
            });
            ArregloTabla.push(arrayOfThisRow);
        }
    });

    $("#" + IdTabla + " tr").each(function () {
        var arrayOfThisRow = [];
        var tableData = $(this).find('td');
        if (tableData.length > 0) {
            tableData.each(function () {
                arrayOfThisRow.push($(this).text());
            });
            ArregloTabla.push(arrayOfThisRow);
        }
    });
    return ArregloTabla;
}

//Capitalizar Cadenas
function CapitalizeMe(Cad_String) {
    var val = Cad_String;
    var newVal = '';
    var val = val.split(' ');
    for (var c = 0; c < val.length; c++) {
        newVal += ((val[c].length > 2) ? val[c].substring(0, 1).toUpperCase() + val[c].substring(1, val[c].length) : val[c]) + ' ';
    }
    Cad_String = newVal;
    return Cad_String
}

function QuitarFormatoDinero(valor) {
    valor = valor.split('.').join('');
    return valor.replace(",", ".").replace('$', '');
}

function RemoverEspaciosBlanco(texto) {
    texto = texto.split(" ").join("");
    return texto;
}
function FormatHoraG(hora) {
    var h = hora.split(":");
    var ampm = h[1].substring(3);
    var hr = h[0];
    var mi = h[1].substring(0, 2);
    if (ampm.toLowerCase() == "pm" && (hr * 1) != 12) {
        hr = (hr * 1) + 12;
    }
    if (ampm.toLowerCase() == "am" && (hr * 1) == 12) {
        hr = "00";
    }
    if (hr.length == 1) {
        hr = "0" + hr;
    }
    return hr + ":" + mi + ":00";
}

Array.prototype.getColumn = function (name) {
    return this.map(function (el) {
        // gets corresponding 'column'
        if (el.hasOwnProperty(name))
            return el[name];
        // removes undefined values
    }).filter(function (el) {
        return typeof el != 'undefined';
    });
};

Array.prototype.unique = function (a) {
    return function () {
        return this.filter(a);
    }
}(function (a, b, c) {
    return c.indexOf(a, b + 1) < 0;
});

Array.prototype.shuffle = function () {
    var counter = this.length, temp, index;
    while (counter > 0) {
        index = (Math.random() * counter--) | 0;
        temp = this[counter];
        this[counter] = this[index];
        this[index] = temp;
    }
}
/* FIN hyoG Rev 29112018 <= FUNCIONES QUE DEVUELVEN VALORES => */


function CopiarAlPortapapeles(tabla_html_id) {
    /* hyoG Rev 29112018 Toma una tabla en HTML por medio del ID y la copia al portapapeles */
    var aux = document.createElement("input");
    aux.setAttribute("value", document.getElementById(tabla_html_id).innerHTML);
    document.body.appendChild(aux);
    aux.select();
    document.execCommand("copy");
    document.body.removeChild(aux);
    jQuery.gritter.add({title: 'Sistema', text: 'La información ha sido copiada al portapapeles', time: '3000', class_name: 'gritter-success'});
}

function deshabilitarF5() {
    /* hyoG Rev 29112018 Verificar Utilidad */
    setTimeout(function () {
        document.onkeydown = function (e) {
            return (e.which || e.keyCode) != 116;
        };
        /* $(document).bind('keyup', 'f5', function(evt) {
         evt.preventDefault()
         
         callback()
         });*/
    }, 500);
}

function habilitarF5() {
    /* hyoG Rev 29112018 Verificar Utilidad */
    document.onkeydown = function (e) {
//return (e.which || e.keyCode) != 116;
    };
}

function initForm() {
    /* hyoG Rev 29112018 Verificar Utilidad */
    $('.init_form').select();
}

function Almacenar() {
    $("#btnActualizar").trigger("click");
}
function Nuevo() {
    $("#btnAgregar").trigger("click");
}
function Eliminar() {
    $("#btnEliminar").trigger("click");
}

function humanizar_horas() {
    $('.humanizar').each(function () {
        $(this).find('#mostrar').html(moment($(this).find('#calcular').html(), 'YYYY-MM-DD h:mm:ss').locale('es').fromNow());
    });
}

function FormatHorasAMPM() {
    $('.pasar24a12horas').each(function () {
        var new_moment = moment(moment().format('YYYY-MM-DD') + ' ' + $(this).html(), 'YYYY-MM-DD hh:mm:ss');
        $(this).html(new_moment.format('hh:mma'));
    });
}

function InsertGridCustom(evtObj, uniqueIndex, rowData, campo) {
    var idAP = evtObj.data.tbWhole.attributes.id.value;
    var rowIndex = $('#' + idAP).appendGrid('getRowIndex', uniqueIndex);
    var newIndex = rowIndex + 1;
    $('#' + idAP).appendGrid('insertRow', 1, newIndex);
    setTimeout(function () {
        var IndexKey = $('#' + idAP).appendGrid('getUniqueIndex', newIndex);
        $('#' + idAP + campo + IndexKey).focus();
    }, 500);
    setTimeout(function () {
        formatear_fechas();
        FormatearChosenAP();
    }, 500);
}

function logonout(Relaod) {
    $.post(rutaAbs + 'scai_arranque/sesiones/salir', function () {
        if (Relaod) {
            location.reload();
        } else {
            $("#mdGuardiaSesion").modal('show').find('.modal-dialog').css({"width": "300px"});
            setTimeout(function () {
                $('#TokenPass').focus();
            }, 1000);
        }
    });
}

var EstadoAyuda = 0;
function MostrarAyuda() {
    if (EstadoAyuda == 0) {
        var cad_ids_help = '';
        $(".help-section").each(function (index) {
            cad_ids_help += $(this).attr('data-id-help') + ',';
        });

        $.ajax({
            url: rutaAbs + "c_maestros/ayuda/cargar_tooltips", type: 'POST',
            data: '&ids=' + cad_ids_help, dataType: 'json',
            success: function (data) {
                $(".help-section").each(function (index) {
                    var bloqueo = $(this).block({
                        css: {
                            border: 'none',
                            color: '#000000',
                            backgroundColor: 'none',
                            '-webkit-border-radius': '5px',
                            '-moz-border-radius': '5px',
                            cursor: 'help'
                                    //opacity: .6
                        },
                        message: '<i class="ace-icon fa fa-question-circle bigger-200"></i>',
                        //onmouseover="mostrar_ayuda(\'' + $(this).attr('data-id-help') + '\')"
                        overlayCSS: {
                            backgroundColor: '#CCCCCC',
                            '-webkit-border-radius': '5px',
                            '-moz-border-radius': '5px',
                            opacity: .3,
                            cursor: 'help'
                        }
                    });

                    var titulo = $(this).attr('data-id-help') + ". ";
                    var cuerpo = "";
                    var ancho = "100px";
                    var posicion = "auto";
                    if (data.toolstips.length > 0) {
                        var LimSup = data.toolstips.length;
                        for (var j = 0; j < LimSup; j++) {
                            if (data.toolstips[j].id == $(this).attr('data-id-help')) {
                                titulo += data.toolstips[j].titulo;
                                cuerpo = data.toolstips[j].cuerpo_html;
                                ancho = data.toolstips[j].ancho;
                                posicion = data.toolstips[j].posicion;
                            }
                        }
                    }

                    bloqueo.popover({
                        title: titulo, html: true, container: "body",
                        trigger: "hover", content: cuerpo, placement: posicion
                    }).on("shown.bs.popover", function () {
                        $(this).data()["bs.popover"].$tip.css("max-width", ancho);
                    });
                });
            }
        });

        EstadoAyuda = 1;
        //$(this).attr('title', "Desactivar Ayuda");
        //$(this).removeClass('btn-success');
    } else {
        $(".help-section").unblock();
        $(".help-section").each(function (index) {
            $(this).popover('destroy');
            $(this).unbind();
        });
        //$(this).addClass('btn-success');
        //$(this).attr('title', "Activar Ayuda");
        EstadoAyuda = 0;
    }
}

function addRules(rulesObj) {
    for (var item in rulesObj) {
        $('#' + item).rules('add', rulesObj[item]);
    }
}

function removeRules(rulesObj) {
    for (var item in rulesObj) {
        $('#' + item).rules('remove');
    }
}

function JVErrorPopover(map, list) {
    /*INICIA LIMPIEZA */
    this.currentElements.removeAttr("title").removeClass("ui-state-highlight input-error tooltip-error").css('border-color', '#D5D5D5').css('background-color', '#FFFFFF');

    $.each(this.currentElements, function (key, lbl) {
        var Obj = $("#" + lbl.id);
        if (Obj.hasClass('force-select2')) {
            Obj = $("#s2id_" + lbl.id);
            Obj.find('.select2-choice').css('border-color', '#D5D5D5').css('background-color', '#FFFFFF');
        }
    });
    /* FINALIZA LIMPIEZA */
    $.each(list, function (index, error) {
        var ObjError = $("#" + error.element.id);
        ObjError.css('border-color', '#F66868').css('background-color', '#FDF5F5');
        if (ObjError.hasClass('force-select2')) {
            var ObjSelect2 = $("#s2id_" + error.element.id);
            ObjSelect2.find('.select2-choice').css('border-color', '#F66868').css('background-color', '#FDF5F5');
            ObjSelect2.attr({"title": error.message, "data-toggle": "tooltip", "data-placement": "top"}).addClass('tooltip-error');
            ObjSelect2.tooltip();
        } else if (ObjError.hasClass('cmbrequired')) {
            var ObjSelect2 = $("#" + error.element.id + "_chosen");
            ObjSelect2.find('.select2-choice').css('border-color', '#F66868').css('background-color', '#FDF5F5');
            ObjSelect2.attr({"title": error.message, "data-toggle": "tooltip", "data-placement": "top"}).addClass('tooltip-error');
            ObjSelect2.tooltip();
        } else {
            ObjError.attr({"title": error.message, "data-toggle": "tooltip", "data-placement": "top"}).addClass("ui-state-highlight input-error tooltip-error");
            ObjError.tooltip();
        }
        /* ObjError.popover('show'); */
    });
}

function GloExportWordExcel(format, name_salida, tabla_id) {
    var doc_type = 'msword';
    var doc_extension = '.doc';
    var doc_name = name_salida;

    switch (format) {
        case 'excel':
            var doc_type = 'msexcel';
            var doc_extension = '.xls';
            break;
    }

    doc_name = doc_name + doc_extension;

    var html = $('#' + tabla_id).clone();
    html.find('button,input,select').remove();
    html.find('table').css({'font-family': 'Tahoma', 'font-size': '12px'});
    html = html.html();

    var blob = new Blob(['\ufeff', html], {
        type: 'application/' + doc_type
    });
    var href = URL.createObjectURL(blob);
    var a = document.createElement('a');
    a.href = href;
    a.download = doc_name;
    document.body.appendChild(a);
    if (navigator.msSaveOrOpenBlob) {
        navigator.msSaveOrOpenBlob(blob, doc_name);
    } else {
        a.click();
    }
    document.body.removeChild(a);
}

function redirFromSquareBtn(elem) {
    /* hyoG Rev 29112018 Verificar Utilidad */
    let url = elem.getAttribute("data-to");
    let v_id = elem.getAttribute("data-v_id");
    if (url.length > 0) {
        $.ajax({
            url: rutaAbs + url,
            type: 'POST',
            success: function (data) {
                $("#contenedor").html(data);
                VolverForm();
                if ((v_id * 1) > 0) {
                    $.ajax({type: 'POST', url: rutaAbs + "auxiliares/setAudiVistas", data: {'id_app_solicitada': v_id}});
                }
            }
        });
    }
}

/*FUNCIONES DE VIDEOLLAMADA*/
function SalaVideoLlamadaRapida(selector_id, altura, key_videoconference) {
    $(selector_id).empty();
    const domain = 'meet.jit.si';
    const options = {
        roomName: key_videoconference,
        height: altura,
        parentNode: document.querySelector(selector_id)
    };
    apiJitsi = new JitsiMeetExternalAPI(domain, options);
}

function CrearSalaVideoLlamadaMaster(selector_id, altura, key_videoconference, user_name) {
    $(selector_id).empty();
    const domain = 'meet.jit.si';
    const options = {
        roomName: key_videoconference,
        height: altura,
        parentNode: document.querySelector(selector_id)
    };
    apiJitsi = new JitsiMeetExternalAPI(domain, options);
    /*AQUI SE DEBE CREAR LAS FUNCIONES DEL MODERADOR COMO SE VE EN EL HOMONIMO DE PRUEBAS*/
    apiJitsi.executeCommand('displayName', user_name);
}

function CrearSalaVideoLlamadaClient(selector_id, altura, key_videoconference, user_name) {
    $(selector_id).empty();
    const domain = 'meet.jit.si';
    const options = {
        roomName: key_videoconference,
        height: altura,
        interfaceConfigOverwrite: {
            /* filmStripOnly: true, */
            TOOLBAR_BUTTONS: [
                'microphone', 'camera', 'closedcaptions', 'desktop', 'fullscreen',
                'fodeviceselection', 'hangup', 'chat',
                'settings', 'raisehand',
                'videoquality', 'filmstrip', 'stats',
                'tileview', 'videobackgroundblur', 'download', 'help'
                        /* , 'mute-everyone' , 'recording', 'livestreaming', 'sharedvideo', 'info', 'profile', 'invite', 'feedback',  'shortcuts', 'etherpad', */
            ],
            SETTINGS_SECTIONS: ['devices', 'language', 'calendar']
        },
        parentNode: document.querySelector(selector_id)
    };
    apiJitsi = new JitsiMeetExternalAPI(domain, options);
    apiJitsi.executeCommand('displayName', user_name);
}

/*PRUEBAS Y PENDIENTES POR MEJORAR*/
function PruebasCrearSalaVideoLlamadaMaster() {
    const domain = 'meet.jit.si';
    const options = {
        roomName: 'PruebaInicial',
        /*width: 700,*/
        height: 600,
        parentNode: document.querySelector('#col_der')
    };
    apiJitsi = new JitsiMeetExternalAPI(domain, options);

    apiJitsi.addEventListener('participantJoined', function (newPerson) {
        alert(newPerson.id);
    });
}

function PruebasCrearSalaVideoLlamadaClient() {
    const domain = 'meet.jit.si';
    const options = {
        roomName: 'PruebaInicial',
        /*width: 700,*/
        height: 600,
        interfaceConfigOverwrite: {filmStripOnly: true},
        parentNode: document.querySelector('#contenedor')
    };
    apiJitsi = new JitsiMeetExternalAPI(domain, options);
}

function EjecutarComando() {
    apiJitsi.executeCommand('toggleFilmStrip');
}

function CerrarVideoLlamada() {
    apiJitsi.dispose();
}