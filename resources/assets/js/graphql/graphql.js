var GraphQl = {}

GraphQl.url = SCAI_GRAPHQL + 'graphql/query';

GraphQl.FormToObject = function(form) {

	formArray = form.serializeArray();
    returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}

GraphQl.Mutation = function(query) {
	return $.ajax({
	  method: "POST",
	  url: GraphQl,
	  data: { query: query }
	})
}
