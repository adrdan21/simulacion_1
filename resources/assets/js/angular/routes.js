angular.module('SCAI').config(['$routeProvider', '$locationProvider', '$ocLazyLoadProvider', function ($routeProvider, $locationProvider, $ocLazyLoadProvider){
  $routeProvider
  .when('/evaluaciones/:inscripcion_id',{
    templateUrl: 'view/ct_evaluacion/ct_evaluacion/evaluacion_disponible_lanzar/9950',
    controller: 'SignupCtrl',
    resolve: {
      loadAsset:['$ocLazyLoad', function($ocLazyLoad) {
        return $ocLazyLoad.load([
          'resources/angular/controller/SignupCtrl.js',
        ])
      }]
    }
  }).when('/lanzar-evaluacion/:evaluacion_id',{
    templateUrl: 'view/registration/index',
    controller: 'SignupCtrl',
    resolve: {
      loadAsset:['$ocLazyLoad', function($ocLazyLoad) {
        return $ocLazyLoad.load([
          'resources/angular/controller/SignupCtrl.js',
        ])
      }]
    }
  });

  $locationProvider.html5Mode(true);
}]);
