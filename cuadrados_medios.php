<?php
$valor = $_POST["numero"];
$filas = $_POST["filas"];
$filas = $filas + 1;
$n = 0;
$extrae = $valor;
$potencia = $extrae * $extrae;
$aleatorio = 0;
?>

<!DOCTYPE html>
<html lang='en'>

<head>
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

</html>

<body>
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <h2 align="center">Cuadrados Medios de <?php echo " [" . $valor . "]" ?></h2>
    </div>
    <div class="col-sm-12">
      <table width="50%" border="2" align="center">
        <tr style="text-align: center">
          <th>Xn </th>
          <th>(Xn - 1)^2 </th>
          <th>Extrae</th>
          <th>Número aleatorio </th>
        </tr>
        <?php
        for ($i = 0; $i < $filas; $i++) {
      if ($i === 0) {
    ?>
        <tr style="text-align: center">
          <td> <?php echo "X" . $n++; ?> </td>
          <td></td>
          <td><?php echo $extrae ?></td>
          <td></td>
        </tr>

        <?php
      } else {
        if ($i > 0) {
        ?>

          <tr style="text-align: center">
            <td> <?php echo "X" . $n++; ?> </td>
            <td><?php echo  $potencia++; ?></td>
            <td><?php echo $extrae ?></td>
            <td><?php echo $aleatorio ?></td>
          </tr>

    <?php

        }
      }

      $potencia =   ($extrae) * ($extrae);
      $parametroExtrae1 = $potencia;
      $parametroExtrae2 = floatval(floor((floatval(strlen($potencia)) - floatval(strlen($valor))) / 2) + 1);
      $parametroExtrae3 = strlen($valor);
      $extrae = (substr($parametroExtrae1, $parametroExtrae2 - 1, $parametroExtrae3)) . PHP_EOL;
      $extrae = floatval($extrae);
      $aleatorio = $extrae / (pow(10, intval(strlen($extrae))));
    }
        ?>
      </table>
    </div>
    <br>
    <div class="col-sm-12">
      <hr/>
      <table align="center">
        <tr>
          <td>
            <input class="btn btn-lg btn-secondary" value="Volver" onClick="history.go(-1);">
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>

</body>

</html>